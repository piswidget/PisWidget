import React, { useState, useEffect } from "react";
import { Modal, Layout, Input, Select, Form, Row, Col, Button } from "antd";
import WidgetValue from "./constants/widgetValue";
import WidgetService from "./services/widgetService";
import WidgetList from "./components/layout/Content";
import { SensorData } from "./type/sensorData";
import { WidgetData, WidgetListType } from "./type/widget";

const { Header } = Layout;
const { Option } = Select;

const App: React.FC = () => {
  const [isModalAddWidgetVisible, setIsModalAddWidgetVisible] = useState(false);
  const [sensorData, setSensorData] = useState<Array<SensorData>>([]);
  const [widgetList, setWidgetList] = useState<Array<WidgetData>>([]);

  const [form] = Form.useForm();

  const getAllSensor = async () => {
    try {
      const res = await WidgetService.getAll();
      setSensorData(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllSensor();
  }, []);

  const showAddWidgetModal = () => {
    setIsModalAddWidgetVisible(true);
  };

  const handleCancel = () => {
    setIsModalAddWidgetVisible(false);
  };

  const handleAddWidget = (values: any) => {
    // WidgetService.create(sensorData.find(sen => {return sen.sensorId === values.sensor }));
    // Need Api to create

    const newWidgetList = [
      ...widgetList,
      {
        title: values.sensorName,
        info: sensorData.find((sen) => {
          return sen.sensorId === values.sensor;
        }),
        type: values.widgetValue,
      } as WidgetData,
    ];

    setWidgetList(newWidgetList);
    setIsModalAddWidgetVisible(false);
  };

  return (
    <Layout
      style={{
        height: "100%",
      }}
    >
      <Header className="header">
        <Button onClick={showAddWidgetModal} type="primary">
          Add
        </Button>
      </Header>

      <Layout>
        <Layout style={{ padding: "0 24px 24px" }}>
          <WidgetList widgetList={widgetList} />
        </Layout>
      </Layout>

      <Modal
        title="Add Widget"
        visible={isModalAddWidgetVisible}
        onOk={() => {
          form
            .validateFields()
            .then((values: any) => {
              form.resetFields();
              handleAddWidget(values);
            })
            .catch((info: any) => {
              console.log("Validate Failed:", info);
            });
        }}
        onCancel={() => {
          form.resetFields();
          handleCancel();
        }}
      >
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          autoComplete="off"
          initialValues={{
            ["widgetValue"]: WidgetValue.singleValue,
            ["sensor"]: sensorData.length > 0 && sensorData[0].sensorId,
          }}
        >
          <Form.Item
            label="Sensor name"
            name="sensorName"
            rules={[{ required: true, message: "Please input sensor name!" }]}
          >
            <Input placeholder="Sensor name" />
          </Form.Item>
          <Form.Item label="Sensor" name="sensor">
            <Select>
              {sensorData &&
                sensorData.map((sensor) => (
                  <Option key={sensor.sensorId} value={sensor.sensorId}>
                    {sensor.sensor}
                  </Option>
                ))}
            </Select>
          </Form.Item>

          <Form.Item label="Widget Type" name="widgetValue">
            <Select defaultValue={WidgetValue.singleValue}>
              <Option value={WidgetValue.singleValue}>
                {WidgetValue.singleValue}
              </Option>
              <Option value={WidgetValue.lineChart}>
                {WidgetValue.lineChart}
              </Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </Layout>
  );
};

export default App;
