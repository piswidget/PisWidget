import axios from "axios";

export default axios.create({
  baseURL: "https://exam-express.vercel.app/api/",
  headers: {
    "Content-type": "application/json"
  }
});