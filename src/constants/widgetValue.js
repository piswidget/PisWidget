const WidgetValue = {
  lineChart: 'Line Chart',
  singleValue: 'Single Value'
};

export default WidgetValue;