import React from "react";
import { Card } from "antd";
import Chart from "react-apexcharts";
import WidgetValue from "../../constants/widgetValue";
import { LineChartData } from "../../type/lineChart";
import { WidgetType } from "../../type/widget";

const Widget = ({ title, info, type }: WidgetType) => {
  const data: LineChartData = {
    series: [
      {
        name: info.sensor,
        data: info.historicalData,
      },
    ],
    options: {
      chart: {
        height: 200,
        type: "line",
        zoom: {
          enabled: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      title: {
        text: `${info.sensor} (${info.unit})`,
        align: "left",
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"],
          opacity: 0.5,
        },
      },
    },
  };

  return (
    <Card
      title={title}
      bordered={true}
      style={{ width: "100%", height: "100%" }}
    >
      {type === WidgetValue.lineChart && (
        <Chart
          options={data.options}
          series={data.series as any}
          type="line"
          width={"100%"}
        />
      )}

      {type === WidgetValue.singleValue && (
        <div>
          <h3>{`${info.sensor} : ${info.value} ${info.unit}`}</h3>
          <h3></h3>
        </div>
      )}
    </Card>
  );
};

export default Widget;
