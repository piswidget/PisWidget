import React from "react";
import { WidgetData, WidgetListType } from "./../../type/widget";
import { Layout, Row, Col } from "antd";
import Widget from "./../common/Widget";
const { Content } = Layout;

const WidgetList = ({ widgetList }: WidgetListType) => {
  return (
    <Layout style={{ padding: "0 24px 24px" }}>
      <Content
        style={{
          background: "#fff",
          padding: 24,
          margin: 0,
          height: "100%",
        }}
      >
        <Row>
          {widgetList &&
            widgetList.map((wid: WidgetData) => (
              <Col xl={6} xs={12} style={{ padding: "5px 8px" }}>
                <Widget title={wid.title} info={wid.info} type={wid.type} />
              </Col>
            ))}
        </Row>
      </Content>
    </Layout>
  );
};

export default WidgetList;
