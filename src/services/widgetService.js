import http from "../http-common";

class WidgetService {
  getAll() {
    return http.get("/sensors");
  }

  create(data) {
    return http.post("/sensors", data);
  }
}

export default new WidgetService();