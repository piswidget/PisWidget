import { SensorData } from "./sensorData";

interface WidgetData {
  title: string;
  info: SensorData;
  type: string;
}

type WidgetType = {
  title: string;
  info: SensorData;
  type: string;
};

export type { WidgetData, WidgetType };
