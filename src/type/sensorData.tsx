interface SensorData {
  sensorId: string;
  sensor: string,
  historicalData: Array<string>,
  value: number,
  unit: string  
}

export type { SensorData };