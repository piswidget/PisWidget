type LineChartData = {
  series: Array<object>;
  options: object;
};

export type { LineChartData };
